package hauptpr�fung2016.BB.gruppe.A;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import java.util.function.IntFunction;

public class Worte {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		Random rand = new Random();

		System.out.print("Zeilen: ");
		int row = scan.nextInt();
		System.out.print("Spalten: ");
		int col = scan.nextInt();
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int[][] matrix = new int[row][col];
		String[][] strarray = new String[row][col];

		int matrixinhalt;
		for (int i = 0; i < matrix.length; i++) {
			int[] getrow = matrix[i];
			for (int j = 0; j < getrow.length; j++) {
				matrix[i][j] = rand.nextInt(27 - 1) + 1;
				matrixinhalt = matrix[i][j];
				System.out.printf("%-2d ", matrixinhalt);
			}
			System.out.println();
		}
		System.out.println("--------------------------------------------------------");
		for (int i = 0; i < strarray.length; i++) {
			String[] numbers = strarray[i];
			for (int j = 0; j < numbers.length; j++) {
				// strarray[i][j] = alphabet.charAt(4);

				int a = matrix[i][j];
				// System.out.println(a);

				char f = alphabet.charAt(a - 1);

				strarray[i][j] = String.valueOf(f);

				String inhalt = strarray[i][j];
				System.out.printf("%-2s ", inhalt);
			}
			System.out.println();
		}

	}

}
