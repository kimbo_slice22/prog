package hauptpr�fung2016.BB.gruppe.A;

public class NurBuchstaben {

	public static void main(String[] args) {
		System.out.println(
				nurBuchstaben(new String[] { "Wort", "K�che", "C3-PO", "Bond, James Bond", "Java", "Das Boot" }));
	}

	public static int nurBuchstaben(String[] worte) {
		// boolean count;
		int counter = 0;
		for (int i = 0; i < worte.length; i++) {
			String wort = worte[i];
			for (int j = 0; j < wort.length(); j++) {
				char buchstabe = wort.charAt(j);
				if ( ! (((buchstabe >= 'A') && (buchstabe <= 'Z')) || ((buchstabe >= 'a') && (buchstabe <= 'z')))) {
					counter++;
				}
			}
		}
		return counter;
	}
}
