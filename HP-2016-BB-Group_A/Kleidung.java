package hauptpr�fung2016.BB.gruppe.A;

import java.util.Arrays;

/*
Kleidung
Franz hat 3 Jacken in den Farben rot, gr�n und gelb, 
5 Hosen in den Farben rot, blau, violett, gr�n und schwarz und 
4 Paar Schuhe in den Farben schwarz, wei�, rot und blau.
Wie viele M�glichkeiten gibt es die Kleidungsst�cke zu kombinieren,
sodass er eine Jacke, eine Hose und ein Paar Schuhe
anhat, aber die Kleidungsst�cke alle eine andere Farbe haben?
 */
public class Kleidung {

	public static void main(String[] args) {
		String[] jacken = { "rot", "gr�n", "gelb" };
		String[] hosen = { "rot", "blau", "violett", "gr�n", "schwarz" };
		String[] schuhe = { "schwarz", "wei�", "rot", "blau" };

		System.out.println("           Kleiderschrank\n" + "------------------------------------");
		System.out.println(Arrays.toString(jacken));
		System.out.println(Arrays.toString(hosen));
		System.out.println(Arrays.toString(schuhe));
		System.out.println("-------------------------------------");

		int count = 0;

		for (int i = 0; i < jacken.length; i++) {
			for (int j = 0; j < hosen.length; j++) {
				for (int k = 0; k < schuhe.length; k++) {
					if (!(jacken[i].equals(hosen[j])) && !(jacken[i].equals(schuhe[k]))
							&& !(hosen[j].equals(schuhe[k]))) {
						count++;
						System.out.printf(
								"Kombi_%-2d ---->  Jackenfarbe: %-9s    Hosenfarbe: %-9s   Schuhfarbe: %-9s%n", count,
								jacken[i], hosen[j], schuhe[k]);

					}

				}
			}

		}
		System.out.printf("%nAnzahl M�glichkeiten: %d ", count);

	}

}
